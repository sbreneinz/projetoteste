package com.manuellemos;

public class Version3 {
	public int v3;

	public Version3(int v3) {
		this.v3 = v3;
	}

	protected int getV3() {
		return v3;
	}

	protected void setV3(int v3) {
		this.v3 = v3;
	}
	
	
}
