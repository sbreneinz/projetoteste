package com.manuellemos;

public class Livro {
	private String nome;
	private int numPaginas;
	private String nomeAutor;
	
	protected String getNome() {
		return nome;
	}
	protected void setNome(String nome) {
		this.nome = nome;
	}
	protected int getNumPaginas() {
		return numPaginas;
	}
	protected void setNumPaginas(int numPaginas) {
		this.numPaginas = numPaginas;
	}
	protected String getNomeAutor() {
		return nomeAutor;
	}
	protected void setNomeAutor(String nomeAutor) {
		this.nomeAutor = nomeAutor;
	}
	
}
